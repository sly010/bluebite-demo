#!/bin/bash

docker run \
	--name bluebite_pg_test \
	-e POSTGRES_USER=bluebite_test \
	-e POSTGRES_PASSWORD=foo9bar \
	-e POSTGRES_POSTGRES_DB=bluebite_test \
	-p 5432:5432 \
	--rm \
	postgres:9.6
