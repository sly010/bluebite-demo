#!/usr/bin/env python

from bluebite.app import App

if __name__ == "__main__":
    app = App()
    app.run()

