#!/usr/bin/env python

from bluebite_tests.utils import TestDBContext
from bluebite_tests.suite_upsert import suite as suite_upsert
from bluebite_tests.suite_flask import suite as suite_flask

import unittest

if __name__ == "__main__":

    # TODO: load this from env
    POSTGRES_USER="bluebite_test"
    POSTGRES_DB="bluebite_test"
    POSTGRES_PASSWORD="foo9bar"

    TESTDB_URI = "postgresql+psycopg2://{user}:{passwd}@localhost/{db}".format(
        user = POSTGRES_USER,
        passwd = POSTGRES_PASSWORD,
        db = POSTGRES_DB,
    )
    print(TESTDB_URI)

    runner = unittest.TextTestRunner()
    with TestDBContext(TESTDB_URI) as dbengine:
        for suite in [
            suite_upsert,
            suite_flask
        ]:
            runner.run(suite(dbengine))


