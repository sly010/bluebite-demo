from flask import Flask
from bluebite.views.upsert import UpsertView
from bluebite.views.search import SearchView

def App(dbe):
    app = Flask(__name__)
    app.add_url_rule('/tags/upsert', view_func = UpsertView.as_view('upsert', dbe = dbe))
    app.add_url_rule('/tags/search', view_func = SearchView.as_view('search', dbe = dbe))
    return app

