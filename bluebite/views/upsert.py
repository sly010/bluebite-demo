from flask import request
from flask import jsonify
from flask import make_response
from flask.views import MethodView

import jsonschema
import jsonschema.exceptions
from bluebite.db import DBSession
from bluebite.models.tag import upsert_tags
from bluebite.models.vendor import Vendor
from .base import DBView

from sqlalchemy.exc import IntegrityError

class UpsertView(DBView, MethodView):

    def transform_meta(self, meta):
        ret = {}
        for m in meta:
            ret[m['key']] = m['value']
        return ret

    def transform(self, payload):
        ret = []
        # TODO: schema does not validate vendor ids
        vendor_id = payload['vendor_id']
        for tag in payload['tags']:
            ret.append({
                "id": tag['tag_id'],
                "vendor_id": vendor_id,
                "meta": self.transform_meta(tag['metadata']),
            })
        return ret, vendor_id

    def post(self):
        payload = request.json
        if not bool(payload):
            return make_response(jsonify(status="invalid input"), 400)

        try:
            jsonschema.validate(payload, schema = upsert_schema)
        except jsonschema.exceptions.ValidationError as e:
            return make_response(jsonify(
                status = "invalid input",
                hint = e.message
            ), 400)

        tags, vendor_id = self.transform(payload)

        with DBSession(self.dbe) as dbs:

            # TODO:
            # in the real world vendor should be already in the db
            # and this section should be removed
            vendor = dbs.query(Vendor).filter_by(id = vendor_id).first()
            if not vendor:
                dbs.add(Vendor(id = vendor_id))
                dbs.flush()

            upsert_tags(dbs, vendor_id, tags)

            return make_response(jsonify(status="ok"), 200)

upsert_schema = {
  "$id": "http://example.com/example.json",
  "type": "object",
  "definitions": {},
  "$schema": "http://json-schema.org/draft-06/schema#",
  "properties": {
    "vendor_id": {
      "$id": "/properties/vendor_id",
      "type": "string",
      "title": "The Vendor_id Schema ",
      "default": "",
      "examples": [
        "76a96528d84544bb9213f122ca55cf3d"
      ]
    },
    "tags": {
      "$id": "/properties/tags",
      "type": "array",
      "items": {
        "$id": "/properties/tags/items",
        "type": "object",
        "properties": {
          "tag_id": {
            "$id": "/properties/tags/items/properties/tag_id",
            "type": "string",
            "title": "The Tag_id Schema ",
            "default": "",
            "examples": [
              "812027ed872a45208bd55a6fefa819c1"
            ]
          },
          "metadata": {
            "$id": "/properties/tags/items/properties/metadata",
            "type": "array",
            "items": {
              "$id": "/properties/tags/items/properties/metadata/items",
              "type": "object",
              "properties": {
                "key": {
                  "$id": "/properties/tags/items/properties/metadata/items/properties/key",
                  "type": "string",
                  "title": "The Key Schema ",
                  "default": "",
                  "examples": [
                    "type"
                  ]
                },
                "value": {
                  "$id": "/properties/tags/items/properties/metadata/items/properties/value",
                  "type": "string",
                  "title": "The Value Schema ",
                  "default": "",
                  "examples": [
                    "shoe"
                  ]
                }
              },
              "required": [
                "key",
                "value"
              ]
            }
          }
        },
        "required": [
          "metadata",
          "tag_id"
        ]
      }
    }
  },
  "required": [
     "vendor_id",
     "tags"
  ]
}

