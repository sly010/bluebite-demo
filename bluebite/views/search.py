from flask import request
from flask import jsonify
from flask import make_response
from flask.views import MethodView
from bluebite.db import DBSession
from bluebite.models.tag import search_tags
import jsonschema
import jsonschema.exceptions

from .base import DBView

class SearchView(DBView, MethodView):

    def post(self):
        payload = request.json
        if not bool(payload):
            return make_response(jsonify(status="invalid input"), 400)

        if 'key' not in payload:
            return make_response(jsonify(status="invalid input"), 400)

        if 'value' not in payload:
            return make_response(jsonify(status="invalid input"), 400)

        with DBSession(self.dbe) as dbs:
            tags = search_tags(dbs, payload['key'], payload['value'])
            return make_response(jsonify(
                status = "ok",
                tags = [ t.asdict() for t in tags ]
            ), 200)
