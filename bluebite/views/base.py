from flask.views import MethodView

class DBView(MethodView):

    def __init__(self, *args, **kwargs):
        self.dbe = kwargs.pop('dbe')
        super().__init__(*args, **kwargs)
