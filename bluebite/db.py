from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager

@contextmanager
def DBSession(engine):
    session = sessionmaker(bind = engine)()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

