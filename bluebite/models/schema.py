from __future__ import unicode_literals
from __future__ import absolute_import

from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base

metadata = MetaData(naming_convention = {
	"ix": 'ix_%(column_0_label)s',
	"uq": "uq_%(table_name)s_%(column_0_name)s",
	"ck": "ck_%(table_name)s_%(constraint_name)s",
	"fk": "fk_%(table_name)s_%(column_0_name)s_refs_%(referred_table_name)s_%(referred_column_0_name)s",
	"pk": "pk_%(table_name)s"
})
BaseModel = declarative_base(metadata = metadata)

