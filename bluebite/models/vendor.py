from . import schema

from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Index
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import HSTORE

class Vendor(schema.BaseModel):

    __tablename__ = "vendor"

    id = Column(UUID, primary_key = True)
    name = Column(String())

