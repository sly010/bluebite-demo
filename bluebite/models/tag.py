from . import schema

from sqlalchemy import Column
from sqlalchemy import Index
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.dialects import postgresql
from sqlalchemy import JSON
from sqlalchemy import cast


class Tag(schema.BaseModel):

    __tablename__ = "tag"

    id = Column(UUID, primary_key = True)

    vendor_id = Column(UUID, ForeignKey('vendor.id'), index = True, nullable = False)
    vendor = relationship('Vendor')
    meta = Column(JSONB)

    __table_args__ = (
        Index('ix_tag_meta', meta, postgresql_using="gin"),
    )

    def asdict(self):
        return {
            "tag_id": self.id,
            "vendor_id": self.vendor_id,
            "meta": self.meta,
        }

# https://www.postgresql.org/docs/9.6/static/sql-insert.html
def upsert_tags(dbs, vendor_id, tags):
    insert_stmt = insert(Tag).values(tags)
    #print(str(insert_stmt.compile(dialect=postgresql.dialect())))
    upsert_stmt = insert_stmt.on_conflict_do_update(
        index_elements = ['id'],
        set_ = dict(meta = insert_stmt.excluded.meta),
        where = (Tag.vendor_id == insert_stmt.excluded.vendor_id)
    )
    #print(str(upsert_stmt.compile(dialect=postgresql.dialect())))
    dbs.execute(upsert_stmt)

def search_tags(dbs, key, value):
    return dbs.query(Tag).filter(Tag.meta[key] == cast(value, JSONB)).all()

