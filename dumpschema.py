#!/usr/bin/env python

import os,sys
sys.path.append(os.path.dirname(__file__))

from sqlalchemy import create_engine
from bluebite.models.schema import BaseModel

def dump(sql, *multiparams, **params):
	print(str(sql.compile(dialect=engine.dialect)) + ";")

if __name__ == "__main__":
	engine = create_engine('postgresql://', strategy='mock', executor=dump, echo=True)
	BaseModel.metadata.create_all(engine)

