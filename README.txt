To run tests, testb need to be running. script included to run testdb
Install dependencies, then run `runtests.py`

You will see some db objects passed around (dbe = dbengine, dbs = dbsession).
I adopted this way of explicitly passing around backend objects in the past and it works well especially if there are multiple backends.
When the proper view classes are used, it's not verbose at all and simplifies testing since you can pass in mock objects and don't need to create an entire test infrastructure.

I decided to make the metadata a JSONB column (instead of a different table) with a GIN index. This places all data close on disk, which improves performance.

The insertion is done with a single giant query akin to:
INSERT INTO "tags" VALUES
(id, vendorid, meta),
(id, vendorid, meta),
(id, vendorid, meta),
(id, vendorid, meta),
(id, vendorid, meta)
ON CONFLICT "id" WHERE ... SET meta = meta;

This does an atomic insert or update, avoids roundtrips, and can insert 10000 rows in 2 seconds on my machine. No need for a queue.


I worked at least 6 hours on this (I know, I am sorry) but it was fun.

TODOS:
- On a real project the endpoints would probably be "/vendors/<vendorid>/tags".
- The search does not filter by vendor id
- On a real project both tests and views could be refactored to use generic classes. I had no time to do that here.
- The uuids are not validated by the schema
- Cleanup imports

