from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from bluebite.models.schema import metadata
from bluebite.db import DBSession

from contextlib import contextmanager

import unittest

class TestDBContext:

    def __init__(self, testdb_uri):
        self.testdb_uri = testdb_uri

    def __enter__(self):
        assert self.testdb_uri.find("test") > -1, "Trying to run tests on a non-test database?"
        engine = create_engine(self.testdb_uri)
        conn = engine.connect()
        conn.execute("drop schema public cascade;");
        conn.execute("create schema public;");
        conn.execute("create extension hstore");
        conn.close()
        metadata.create_all(engine)
        return engine

    def __exit__(self, type, value, traceback):
        pass

class DBTestCase(unittest.TestCase):
    def __init__(self, db_engine):
        self.db_engine = db_engine
        super().__init__()


