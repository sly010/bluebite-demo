import unittest

from .utils import DBTestCase
from .utils import DBSession
from bluebite.models import Vendor
from bluebite.models.tag import Tag, upsert_tags, search_tags
from bluebite.app import App
import uuid
import json

class UpsertBadCases(DBTestCase):

    def runTest(self):
        app = App(self.db_engine)
        client = app.test_client()

        # invalid method
        r = client.get("/tags/upsert")
        assert r.status_code == 405

        # empty body
        r = client.post("/tags/upsert")
        assert r.status_code == 400

        # invalid body
        r = client.post("/tags/upsert", data = "MEH")
        assert r.status_code == 400

class UpsertGoodCases(DBTestCase):
    def runTest(self):
        app = App(self.db_engine)
        client = app.test_client()

        for case in [
            "bluebite_tests/5d207da03b0040578e4c5160597357b7.json",
            "bluebite_tests/7447156584c543658455558747c64d2c.json",
            "bluebite_tests/a3e3853750724e2994515bb70d646c32.json",
            # this last one doesn't actually pass schema validation
            #"bluebite_tests/995c3f51ebd7486695d8947152bb38d3.json",
        ]:
            r = client.post("/tags/upsert", json = json.load(open(case)))
            assert r.status_code == 200

class SearchBadCases(DBTestCase):
    def runTest(self):
        app = App(self.db_engine)
        client = app.test_client()

        r = client.post("/tags/search")
        assert r.status_code == 400

        r = client.post("/tags/search", json = { "value": "A" })
        assert r.status_code == 400

        r = client.post("/tags/search", json = { "key": "A" })
        assert r.status_code == 400

class SearchGoodCases(DBTestCase):
    def runTest(self):

        # adding tags
        with DBSession(self.db_engine) as dbs:

            vendor_id = uuid.uuid4().hex
            tag_id = uuid.uuid4().hex

            dbs.add(Vendor(
                id = vendor_id,
                name = "Sly",
            ))
            dbs.flush()

            upsert_tags(dbs, vendor_id,
                (
                    [ { "id": uuid.uuid4().hex, "vendor_id": vendor_id, "meta": { "A": 5, "B": 3 }} for x in range(0,10) ] +
                    [ { "id": tag_id,           "vendor_id": vendor_id, "meta": { "A": 2, "C": 5 }} for x in range(0,1) ]
                )
            )

        app = App(self.db_engine)
        client = app.test_client()

        r = client.post("/tags/search", json = { "key": "A", "value": 5 })
        assert r.status_code == 200
        json.loads(r.data)

def suite(dbe):
    s = unittest.TestSuite()
    s.addTest(UpsertBadCases(dbe))
    s.addTest(UpsertGoodCases(dbe))
    s.addTest(SearchBadCases(dbe))
    s.addTest(SearchGoodCases(dbe))
    return s

