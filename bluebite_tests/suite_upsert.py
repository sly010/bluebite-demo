import unittest

from .utils import DBTestCase
from .utils import DBSession
from bluebite.models import Vendor
from bluebite.models.tag import Tag, upsert_tags, search_tags

import uuid

class TestUpsertAndSearch(DBTestCase):

    def runTest(self):
        with DBSession(self.db_engine) as dbs:

            vendor_id = uuid.uuid4().hex
            tag_id = uuid.uuid4().hex

            dbs.add(Vendor(
                id = vendor_id,
                name = "Sly",
            ))
            dbs.flush()

            upsert_tags(dbs, vendor_id,
                (
                    [ { "id": uuid.uuid4().hex, "vendor_id": vendor_id, "meta": { "A": 5, "B": 3 }} for x in range(0,10) ] +
                    [ { "id": tag_id,           "vendor_id": vendor_id, "meta": { "A": 2, "C": 5 }} for x in range(0,1) ]
                )
            )

            upsert_tags(dbs, vendor_id,
                (
                    [ { "id": uuid.uuid4().hex, "vendor_id": vendor_id, "meta": { "A": 5, "B": 5 }} for x in range(0,10) ] +
                    [ { "id": tag_id,           "vendor_id": vendor_id, "meta": { "A": 6, "B": 3 }} for x in range(0,1) ]
                )
            )

            assert len(search_tags(dbs, "A", 3)) == 0
            assert len(search_tags(dbs, "A", 2)) == 0
            assert len(search_tags(dbs, "A", 6)) == 1
            assert len(search_tags(dbs, "B", 3)) == 11

def suite(db_engine):
    s = unittest.TestSuite()
    s.addTest(TestUpsertAndSearch(db_engine))
    return s

